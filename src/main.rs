fn main() {
    let arr = vec![6, 5, 3, 8, 7, 2, 4];
    //let arr = vec![1, 2, 3, 4, 5, 6, 7];
    println!("Original order:");
    print_vec(&arr);

    let show_debug = true;
    let bubble_sorter = BubbleSorter{show_debug: show_debug};
    let insertion_sorter = InsertionSorter{show_debug: show_debug};
    let merge_sorter = MergeSorter{show_debug: show_debug};

    let mut sorter: &dyn Sorter = &bubble_sorter;
    let sorted = sorter.sort(&arr);
    println!("Bubble sorted order:");
    print_vec(&sorted);

    sorter = &insertion_sorter;
    let sorted = sorter.sort(&arr);
    println!("Insertion sorted order:");
    print_vec(&sorted);

    sorter = &merge_sorter;
    let sorted = sorter.sort(&arr);
    println!("Merge sorted order:");
    print_vec(&sorted);
}

fn print_vec(arr: &Vec<i32>) {
    for number in arr
    {
        println!("{}", number);
    }  
}

pub trait Sorter {
    fn sort(&self, arr: &Vec<i32>) -> Vec<i32>;
}

pub struct BubbleSorter {
    show_debug: bool
}

impl Sorter for BubbleSorter {
    fn sort(&self, arr: &Vec<i32>) -> Vec<i32> {
        let mut sorted = arr.to_vec();
        let mut pass = 0;
        loop {
            let mut did_swap = false;
            for i in 0..(sorted.len()-1) {
                if sorted[i] > sorted[i+1]{
                    sorted.swap(i, i+1);
                    did_swap = true;
                }
            }
            pass += 1;
            if self.show_debug {
                println!("Sorted order after pass {}:", pass);
                print_vec(&sorted);
            }
            if !did_swap {
                break;
            }
        }
        sorted 
    }    
}

pub struct InsertionSorter {
    show_debug: bool
}

impl Sorter for InsertionSorter {
    fn sort(&self, arr: &Vec<i32>) -> Vec<i32> {
        if self.show_debug {
            println!("Insertion sort debug prints");
        }
        let mut sorted = arr.to_vec();
        for i in 1..(sorted.len()) {
            let number_to_place = sorted[i];
            let original_index = i;
            if self.show_debug {
                println!("number to place {}", number_to_place);
                println!("original index {}", original_index);
            }
            for u in (0..i).rev() {
                if self.show_debug {
                    println!("test_index {}", u);
                }
                if sorted[u] <= number_to_place {
                    break;
                }
                else {
                    sorted.swap(u, u+1);
                }
            }
            if self.show_debug {
                println!("Sorted order after pass {}:", i);
                print_vec(&sorted)
            }
        }
        sorted 
    }
}

pub struct MergeSorter {
    show_debug: bool
}

impl Sorter for MergeSorter {
    fn sort(&self, arr: &Vec<i32>) -> Vec<i32> {
        if self.show_debug {
            println!("Merge sort debug prints");
        }
        let mut sorted = arr.to_vec();
        let end_index = sorted.len() - 1;
        self.merge_sort(&mut sorted, 0, end_index);
        sorted 
    }

}

impl MergeSorter {
    fn merge_sort(&self, arr: &mut Vec<i32>, start_index: usize, end_index: usize) {
        let length = end_index - start_index + 1;
        if length < 2 {
            // One number is always sorted
        } else if length == 2 {
            // Swap two numbers if necessary
            if arr[start_index] > arr[end_index] {
                arr.swap(start_index, end_index)
            }
        } else {
            let left_end_index = length / 2 - 1 + start_index;
            let right_start_index = left_end_index + 1;

            // Split into two and sort both sides O(n log n)
            self.merge_sort(arr, start_index, left_end_index);
            self.merge_sort(arr, right_start_index, end_index);

            // Merge the sorted sides O(n)
            self.merge(arr, start_index, left_end_index, right_start_index, end_index)
        }
    }

    fn merge(&self, arr: &mut Vec<i32>, left_start_index: usize, left_end_index: usize, right_start_index: usize, right_end_index: usize) {
        // Make copy of the slices so merge result can be saved into arr
        let left_slice = arr[left_start_index..left_end_index+1].to_vec();
        let right_slice = arr[right_start_index..right_end_index+1].to_vec();

        let mut left_index = 0;
        let mut right_index = 0;
        let mut merged_index = left_start_index;

        // Walk the sides by stepping forward on the side that has smaller number
        loop {
            let numbers_left_on_left = left_index < left_slice.len();
            let numbers_left_on_right = right_index < right_slice.len();
            if !numbers_left_on_right || (numbers_left_on_left && left_slice[left_index] < right_slice[right_index]) {
                arr[merged_index] = left_slice[left_index];
                left_index += 1;
                merged_index += 1
            } else {
                arr[merged_index] = right_slice[right_index];
                right_index += 1;
                merged_index += 1
            }

            // Terminate when all numbers have been processed
            if merged_index > right_end_index {
                break;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn is_sorted(arr: &Vec<i32>) -> bool {
        let mut result = true;
        for i in 0..(arr.len()-1) {
            if arr[i] > arr[i+1]{
                result = false;
                break;
            }
        }
        result
    }

    #[test]
    fn test_bubble_sort() {
        let arr = vec![5, 1, 4, 2, 8];
        let sorter = BubbleSorter{show_debug: false};
        let sorted = sorter.sort(&arr);
        assert!(is_sorted(&sorted));   
    }

    #[test]
    fn test_insertion_sort() {
        let arr = vec![5, 1, 4, 2, 8];
        let sorter = InsertionSorter{show_debug: false};
        let sorted = sorter.sort(&arr);
        assert!(is_sorted(&sorted));
    }

    #[test]
    fn test_merge_sort() {
        let arr = vec![5, 1, 4, 2, 8];
        let sorter = MergeSorter{show_debug: false};
        let sorted = sorter.sort(&arr);
        assert!(is_sorted(&sorted));   
    }
}